# jupyterlab_enkiintro

Show an intro widget in the laucher that displays ENKI information.


## Prerequisites

* JupyterLab > 3.0

## Installation

To install the labextension locally, enter the following in your terminal:
```bash
jupyter labextension install .
```
To install the extension from the npm repository, enter the following in your tewrminal:
```
[sudo] jupyter labextension install @enki-portal/enkiintro
```

## Contributing

### Install

The `jlpm` command is JupyterLab's pinned version of
[yarn](https://yarnpkg.com/) that is installed with JupyterLab. You may use
`yarn` or `npm` in lieu of `jlpm` below. Open a terminal window and execute the following in the repository directory:

```bash
# Clone the repo to your local environment
# Move to jupyterlab_test directory
# Install dependencies
jlpm
# Build Typescript source
jlpm build
# Link your development version of the extension with JupyterLab
jupyter labextension link .
# Rebuild Typescript source after making changes
jlpm build
# Rebuild JupyterLab after making any changes
jupyter lab build
```
You can watch the source directory and run JupyterLab in watch mode to watch for changes in the extension's source and automatically rebuild the extension and application.

```bash
# Watch the source directory in another terminal tab
jlpm watch
# Run jupyterlab in watch mode in yet another terminal tab
jupyter lab --watch
```

### Uninstall

```bash
jupyter labextension uninstall jupyterlab_test
```

## Publish the package to npmjs.org

*npm* is both a JavaScript package manager and the de facto registry for JavaScript software. You can sign up for an account on the *npmjs.com* site or create an account from the command line by running
```
npm adduser
```
and entering values when prompted.

Next, open the project ```package.json``` file in your text editor. Prefix the name field value with ```@your-npm-username>/`` so that the entire field reads ```"name": "@your-npm-username/enkiintro"``` where you’ve replaced the string ```your-npm-username``` with your real username.  

Review the homepage, repository, license, and other supported package.json fields while you have the file open. Then open the README.md file and adjust the command in the Installation section so that it includes the full, username-prefixed package name you just included in the package.json file. For example:
```
jupyter labextension install @enki-portal/enkiintro
```
Now run the following command to publish your package:
```
npm publish --access=public
```
Check that your package appears on the npm website. You can either search for it from the homepage or visit https://www.npmjs.com/package/@enki-portal/enkiintro directly. If it doesn’t appear, make sure you’ve updated the package name properly in the package.json and run the npm command correctly. Compare your work with the state of the reference project at the 06-prepare-to-publish tag for further debugging.